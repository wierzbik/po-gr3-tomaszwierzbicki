package pl.imiajd.Wierzbicki;

import java.time.LocalDate;
import java.util.Objects;

abstract class Instrument {
    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public abstract String dzwiek();

    public String getProducent(){
        return producent;
    }

    public LocalDate getRokProdukcji(){
        return rokProdukcji;
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Instrument that = (Instrument) o;
        return Objects.equals(producent, that.producent) && Objects.equals(rokProdukcji, that.rokProdukcji);
    }


    public String toString() {
        return "Instrument{" + "producent='" + producent + '\'' + ", rokProdukcji=" + rokProdukcji + '}';
    }

    private String producent;
    private LocalDate rokProdukcji;
}
