﻿package pl.imiajd.Wierzbicki;

import java.time.LocalDate;

public class TestOsoba {

    public static void main(String[] args) {
	    Osoba[] ludzie = new Osoba[2];
	    ludzie[0] = new Pracownik("Bartek Borysiuk", 50000, LocalDate.of(1950,12,6));
	    ludzie[1] = new Student("Lech Lumpik", "Matematyka Stosowana", 3.92);

	    for(Osoba p: ludzie){
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
        System.out.println("-----------");
        Osoba p1 = new Student ("Anna Bes", "Informatyka", 4.22);
        System.out.println( ((Student) p1).getSredniaOcen());
        System.out.println(((Student) p1).setSredniaOcen(4.11));

        System.out.println("-----------");
        String[] imie = {"Franek","Marek"};
        Osoba p2 = new Pracownik("Kowalski", 50000, LocalDate.of(1967,9,2));
        System.out.println(((Pracownik) p2).getDataZatrudnienia());

    }
}
