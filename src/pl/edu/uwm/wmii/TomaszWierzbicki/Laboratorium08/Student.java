package pl.imiajd.Wierzbicki;

public class Student extends Osoba{
    public Student(String nazwisko, String kierunek, double sredniaOcen){
        super(nazwisko);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis(){
        return "kierunek studiów: " + kierunek;
    }

    public double getSredniaOcen(){
        return sredniaOcen;
    }

    public double setSredniaOcen(double srednia){
        this.sredniaOcen = srednia;
        return sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;
}
