﻿package pl.imiajd.Wierzbicki;

import java.time.LocalDate;

public class Pracownik extends Osoba {
    public Pracownik(String nazwisko, double pobory, LocalDate dataZatrudnienia){
        super(nazwisko);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory(){
        return pobory;
    }

    public LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }

    public String getOpis(){
        return String.format("pracownik z pensja %.2f zl", pobory);
    }
    private double pobory;
    private LocalDate dataZatrudnienia;
}
