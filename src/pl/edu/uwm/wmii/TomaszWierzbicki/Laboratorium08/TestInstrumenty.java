package pl.imiajd.Wierzbicki;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {

    public static void main(String[] args) {

        Instrument[] lista = new Instrument[5];
        lista[0] = new Flet ("Klasiko", LocalDate.of(1999,5,6));
        lista[1] = new Fortepian ("Barokowo", LocalDate.of(1980,4,27));
        lista[2] = new Skrzypce ("Renesanso", LocalDate.of(2005,11,16));
        lista[3] = new Flet("Klasiko", LocalDate.of(1987,2,1));
        lista[4] = new Fortepian ("Renesansowo", LocalDate.of(2012,11,12));

        ArrayList <Instrument> Orkiestra = new ArrayList<Instrument>();
        for(int i = 0; i < lista.length; i++)
            Orkiestra.add(lista[i]);

        for(Instrument p: Orkiestra ){
            System.out.println(p.dzwiek());
        }
        System.out.println("----------");
        for(Instrument x: Orkiestra ){
            System.out.println(x.toString());
        }

    }
}
