package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;

public class Zadanie1d {

    public static String repeat(String str,int n){
        String wynik = "";
        for(int i=0;i<n;i++){
            wynik+=str;
        }
        return wynik;
    }

    public static void main(String[] args) {
        Scanner z = new Scanner(System.in);
        String str = z.nextLine();
        System.out.println(repeat(str,4));

    }
}
