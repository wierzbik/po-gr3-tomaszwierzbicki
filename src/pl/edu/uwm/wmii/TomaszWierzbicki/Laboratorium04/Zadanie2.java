package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.String;

public class Zadanie2 {

    public static int IleWystapien(String plik,char znak) throws IOException{
        FileReader filereader = new FileReader(plik);
        BufferedReader bufferedReader = new BufferedReader(filereader);
        Zadanie1a count = new Zadanie1a();
        int ilosc =0;
        try{
            String text = bufferedReader.readLine();
            do{
                int wynik = count.countChar(text,znak);
                ilosc += wynik;
                text = bufferedReader.readLine();
            }while(text !=null);
        }finally {
            bufferedReader.close();
        }
        return ilosc;
    }

    public static void main(String[] args)  throws IOException {
        System.out.println(IleWystapien("Zad2",'n'));

    }
}
