package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Zadanie3 {
    public static int WystapieniaWyrazu(String plik,String wyraz)throws IOException {
        FileReader fileReader = new FileReader(plik);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        Zadanie1b count = new Zadanie1b();
        int ilosc =0;
        try{
            String text = bufferedReader.readLine();
            do{
                int wynik = count.countSubStr(text,wyraz);
                ilosc+=wynik;
                text = bufferedReader.readLine();
            }while(text !=null);
        }finally {
            bufferedReader.close();
        }
        return ilosc;
    }

    public static void main(String[] args) throws IOException {
        System.out.println(WystapieniaWyrazu("zad3","ok"));

    }
}
