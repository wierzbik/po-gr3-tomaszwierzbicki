package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;

public class Zadanie1c {
    public static String middle(String str){
        int pozycja=0;
        int rozmiar=0;
        if(str.length()%2 ==0){
            pozycja = str.length()/2-1;
            rozmiar = 2;
        }
        else{
            pozycja = str.length()/2;
            rozmiar =1;
        }
        return str.substring(pozycja,rozmiar+pozycja);
    }

    public static void main(String[] args) {
        Scanner z = new Scanner(System.in);
        String str = z.nextLine();
        System.out.println(middle(str));
    }
}
