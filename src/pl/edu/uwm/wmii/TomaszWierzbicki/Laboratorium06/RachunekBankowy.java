package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;

public class RachunekBankowy {

    static double rocznaStopaProcentowa;
    private double saldo;
    public void stanKonta(double kwota){
        saldo = kwota;
    }
    public double obliczMiesieczneOdsetki(){
        double wynik =  (saldo*rocznaStopaProcentowa)/12;
        double stanKonta = saldo+wynik;
        System.out.println("Saldo konta przed doliczeniem odsetek: "+saldo);
        System.out.println("Saldo konta po doliczeniu odsetek: "+stanKonta);
        return stanKonta;
    }
    static void SetRocznaStopeProcentowa(double stopaProcentowa){
        rocznaStopaProcentowa = stopaProcentowa;
    }

    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy();
        RachunekBankowy saver2 = new RachunekBankowy();
        saver1.stanKonta(2000);
        RachunekBankowy.SetRocznaStopeProcentowa(0.04);
        System.out.println("-----------------------------------");
        System.out.println("Oprocentowanie 4%");
        saver1.obliczMiesieczneOdsetki();
        System.out.println("-----------------------------------");
        System.out.println("Oprocentowanie 4%");
        saver2.stanKonta(3000);
        saver2.obliczMiesieczneOdsetki();
        System.out.println("-----------------------------------");
        System.out.println("Oprocentowanie 5%");
        RachunekBankowy.SetRocznaStopeProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        System.out.println("-----------------------------------");
        System.out.println("Oprocentowanie 5%");
        saver2.obliczMiesieczneOdsetki();
    }
}