package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.ArrayList;
import java.lang.StringBuffer;

public class IntegerSet {
    boolean zbior[] = new boolean[100];

    public boolean[] getZbior() {
        for (int i = 0; i < 100; i++)
        {
            zbior[i] = false;
        }
        return zbior;


    }
    public void SprawdzElement(int Element[]) {
        for (int i = 1; i < 100; i++) {
            for (int j = 0; j < Element.length; j++) {
                if (Element[j] == i) {
                    zbior[i - 1] = true;
                }
            }
        }
    }

    public static void union(boolean zbior1[] , boolean zbior2[]) {
        int licznik=0;
        for(int i=0;i<100;i++) {
            if (zbior1[i] == false || zbior2[2] == true) {
                licznik++;
            }
        }
        int [] sMnogosciowa= new int[licznik];
        int ile=0;
        for(int i=0; i<100; i++)
        {
            if(zbior1[i]==true || zbior2[i]==true){
                sMnogosciowa[ile++]=i+1;
                System.out.print(sMnogosciowa[ile-1]+" ");
            }
        }
        System.out.println();
    }
    public static void intersection (boolean zbior1[],boolean zbior2[]){
        ArrayList iloczyn=new ArrayList();
        for(int i=0 ; i<100; i++)
        {
            if(zbior1[i]==true && zbior2[i]==true)
            {
                iloczyn.add(i+1);
            }
        }
        for(int i=0;i<iloczyn.size(); i++)
        {
            System.out.print(iloczyn.get(i)+" ");
        }
        System.out.println();
    }
    public  int [] insertElement(int []zbior,int liczba){
        System.out.println("Dodano element : "+liczba);
        int dlugosc=zbior.length+1;
        int tmp[]=new int[dlugosc];
        for(int i=0; i<zbior.length; i++)
        {
            tmp[i]=zbior[i];
        }
        tmp[dlugosc-1]=liczba;
        return tmp;
    }
    public  int [] deleteElement(int []zbior,int liczba){
        int dlugosc=0;
        for(int i=0; i<zbior.length; i++)
        {
            if(zbior[i]!=liczba){
                dlugosc++;
            }
        }
        int ile=0;
        int tmp[]=new int[dlugosc];
        for(int i=0; i<zbior.length; i++)
        {
            if(zbior[i]!=liczba)
            {
                tmp[ile]=zbior[i];
                ile++;
            }
        }

        if(dlugosc!=zbior.length)
        {
            System.out.println("Usunieto : "+liczba);
        }
        else {
            System.out.println("Element nie znajduje sie w zbiorze");
        }

        return tmp;
    }
    public String toString(){
        System.out.print("Zbior : ");
        StringBuffer buff=new StringBuffer();
        buff.append("( ");
        for(int i=0 ;i<100; i++) {
            if(zbior[i]==true)
            {
                int a=i+1;
                buff.append(a).append(" ");
            }
        }
        buff.append(" )");
        System.out.println(buff);
        return buff.toString();
    }
    public boolean equals(boolean zbior[]){
        for(int i=0; i<100;i++)
        {
            if(zbior[i]!=true && zbior[i]!=true)
            {
                return false;
            }
        }
        return true;
    }
    public void wyswietl(int tab[]){
        System.out.print("Zbior: ");

        for(int i=0 ;i<tab.length; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.println();
    }
    public static void main(String[] args) {
        int zbior[]={6,22,1,432,-11,79,34,5,33,54,5,7,8,1};
        int zbiorB[]={12,44,1,332,-22,77,2,11};

        IntegerSet x=new IntegerSet();
        x.wyswietl(zbior);
        x.getZbior();
        x.SprawdzElement(zbior);

        x.toString();
        boolean tab[]=x.zbior;
        System.out.println();
        IntegerSet y=new IntegerSet();
        y.wyswietl(zbiorB);
        y.getZbior();
        y.SprawdzElement(zbiorB);
        System.out.print("Suma zbiorow: ");

        union(x.zbior,y.zbior);
        System.out.print("Iloczyn zbiorow: ");
        intersection(x.zbior,y.zbior);
        zbior=x.insertElement(zbior,322);
        x.wyswietl(zbior);
        x.SprawdzElement(zbior);
        int zb[]=zbior;
        zb=x.deleteElement(zbior,33);
        x.wyswietl(zb);
        x.getZbior();
        x.SprawdzElement(zb);
        System.out.println((x.zbior).equals(y.zbior));


    }

}