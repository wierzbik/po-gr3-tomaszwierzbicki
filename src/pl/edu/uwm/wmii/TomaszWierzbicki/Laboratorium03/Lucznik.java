package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;

public class Lucznik {
    String imie = "OrkA";
    private double zywotnosc = 100.0D;
    private int zrecznosc = 15;
    int PT = 3;
    double moc_ataku;

    public Lucznik() {
    }

    public void zywotnosc(double x) {
        this.zywotnosc = x;
        if (0.0D > x) {
            this.zywotnosc = 0.0D;
        }

        if (100.0D < x) {
            this.zywotnosc = 100.0D;
        }

    }

    public void moc_ataku() {
        this.moc_ataku = (double)(this.zrecznosc * this.PT) * this.zywotnosc;
    }

    public String toString() {
        return " imie: " + this.imie + "\n życie: " + this.zywotnosc+ "\n sila: " + this.zrecznosc + "\n PT: " + this.PT + "\n moc ataku: " + this.moc_ataku;
    }
    public static void main(String[] args) {
        Scanner odczyt = new Scanner(System.in);
        Lucznik luk = new Lucznik();
        luk.moc_ataku();
        luk.imie = "Legolas";
        System.out.println("\n");
        System.out.println(luk.toString());

        System.out.println("Podaj wartośc hp: ");
        double n = odczyt.nextDouble();
        luk.zywotnosc(n);
        luk.moc_ataku();
        System.out.println("przeciążony to string:");
        System.out.println("\n");
        System.out.println(luk.toString());
    }
}



