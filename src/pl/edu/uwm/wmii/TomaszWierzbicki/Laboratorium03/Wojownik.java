package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;

public class Wojownik {
    String imie = "OrkA";
    double zywotnosc = 100.0D;
    int sila = 15;
    int PT = 1;
    double moc_ataku;

    public Wojownik() {
    }

    public void zywotnosc(double x) {
        this.zywotnosc = x;
        if (0.0D > x) {
            this.zywotnosc = 0.0D;
        }

        if (100.0D < x) {
            this.zywotnosc = 100.0D;
        }

    }

    public void moc_ataku() {
        if (this.zywotnosc < 20.0D) {
            this.moc_ataku = (double) (this.sila * this.PT) * this.zywotnosc * 1.5D;
        } else {
            this.moc_ataku = (double) (this.sila * this.PT) * this.zywotnosc;
        }

    }


    public String toString() {
        return " imie: " + this.imie + "\n życie: " + this.zywotnosc + "\n sila: " + this.sila + "\n PT: " + this.PT + "\n moc ataku: " + this.moc_ataku;
    }
    public static void main(String[] args) {
        Scanner odczyt = new Scanner(System.in);
        Wojownik woj = new Wojownik();
        woj.moc_ataku();
        woj.imie = "Asagon";;
        System.out.println(woj.toString());
        System.out.println("\n");

        System.out.println("Podaj wartośc hp: ");
        double n = odczyt.nextDouble();
        woj.zywotnosc(n);
        woj.moc_ataku();
        System.out.println("przeciążony to string:");
        System.out.println(woj.toString());
        System.out.println("\n");
    }
}