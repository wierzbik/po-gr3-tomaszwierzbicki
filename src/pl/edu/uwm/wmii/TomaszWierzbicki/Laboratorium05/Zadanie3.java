package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.ArrayList;
import java.util.Collections;

public class Zadanie3 {

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {

        int dlugosc1 = a.size();
        int dlugosc2 = b.size();
        Collections.sort(a);
        Collections.sort(b);
        ArrayList<Integer> sortowana = new ArrayList<>();
        int i = 0, j = 0, k = 0;

        while (i < dlugosc1 && j < dlugosc2) {

            if (a.get(i) < b.get(j))
                sortowana.add(k++,a.get(i++));
            else
                sortowana.add(k++,b.get(j++));
        }

        while (i < dlugosc1) {
            sortowana.add(k++, a.get(i++));
        }

        while (j < dlugosc2) {
            sortowana.add(k++, b.get(j++));
        }
        return sortowana;
    }

    public static void main(String[] args) {
        ArrayList<Integer> pierwszy =new ArrayList<>();
        ArrayList<Integer> drugi =new ArrayList<>();
        pierwszy.add(0,1);
        pierwszy.add(1,4);
        pierwszy.add(2,9);
        pierwszy.add(3,16);
        drugi.add(0,9);
        drugi.add(1,7);
        drugi.add(2,4);
        drugi.add(3,9);
        drugi.add(4,11);
        System.out.println(mergeSorted(pierwszy,drugi));
    }
}
