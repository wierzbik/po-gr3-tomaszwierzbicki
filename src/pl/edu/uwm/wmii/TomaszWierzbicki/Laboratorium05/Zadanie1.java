package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.ArrayList;

public class Zadanie1 {
    public static ArrayList<Integer>append(ArrayList<Integer>a,ArrayList<Integer>b){
        ArrayList<Integer> scalone = new ArrayList<>();
        int rozmiar = a.size()+b.size();
        for(int i=0;i<a.size();i++){
            scalone.add(a.get(i));
        }
        for(int i=a.size();i<rozmiar;i++){
            scalone.add(b.get(i-a.size()));
        }
        return scalone;
    }

    public static void main(String[] args) {
        ArrayList <Integer> a = new ArrayList<>();
        ArrayList <Integer> b = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        a.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        System.out.println(append(a,b));
    }
}