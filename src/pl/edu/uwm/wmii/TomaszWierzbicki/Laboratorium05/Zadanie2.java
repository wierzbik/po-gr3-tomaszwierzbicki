package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.ArrayList;

public class Zadanie2 {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> wynik = new ArrayList<>();
        int dlugosc1 = a.size();
        int dlugosc2 = b.size();
        for(int i = 0; i<Math.min(dlugosc1,dlugosc2);i++) {
            wynik.add(a.get(i));
            wynik.add(b.get(i));
        }
        if(dlugosc1>dlugosc2)
        {
            for(int i = dlugosc2; i<dlugosc1;i++) {
                wynik.add(a.get(i));
            }
        }
        else if(dlugosc2>dlugosc1)
        {
            for(int i = dlugosc1; i<dlugosc2;i++) {
                wynik.add(b.get(i));
            }
        }
        return wynik;
    }

    public static void main(String[] args) {
        ArrayList<Integer> pierwszy =new ArrayList<>();
        ArrayList<Integer> drugi =new ArrayList<>();
        pierwszy.add(0,1);
        pierwszy.add(1,4);
        pierwszy.add(2,9);
        pierwszy.add(3,16);
        drugi.add(0,9);
        drugi.add(1,7);
        drugi.add(2,4);
        drugi.add(3,9);
        drugi.add(4,11);
        System.out.println(merge(pierwszy,drugi));
    }
}
