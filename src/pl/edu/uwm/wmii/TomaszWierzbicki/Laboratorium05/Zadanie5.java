package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.ArrayList;

public class Zadanie5 {

    public static void reverse(ArrayList<Integer> a) {
        ArrayList<Integer> lista = new ArrayList<>();
        int rozmiarA = a.size();
        for(int i=rozmiarA-1;i>=0;i--) {
            lista.add(a.get(i));
        }
        int rozmiar = lista.size();
        a.removeAll(a);
        for(int i=0;i<rozmiar;i++) {
            a.add(lista.get(i));
        }
    }

    public static void main(String[] args) {
        ArrayList<Integer> pierwszy =new ArrayList<>();
        ArrayList<Integer> drugi =new ArrayList<>();
        pierwszy.add(0,1);
        pierwszy.add(1,4);
        pierwszy.add(2,9);
        pierwszy.add(3,16);
        drugi.add(0,9);
        drugi.add(1,7);
        drugi.add(2,4);
        drugi.add(3,9);
        drugi.add(4,11);
        reverse(pierwszy);
        System.out.println(pierwszy);
    }
}
