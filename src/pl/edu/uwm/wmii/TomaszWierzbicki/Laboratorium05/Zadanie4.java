package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.ArrayList;
public class Zadanie4 {

    public static ArrayList<Integer> reversed(ArrayList<Integer> a) {
        ArrayList<Integer> odwrocona = new ArrayList<>();
        int rozmiar = a.size();
        for(int i=rozmiar-1;i>=0;i--) {
            odwrocona.add(a.get(i));
        }
        return odwrocona;
    }

    public static void main(String[] args) {
        ArrayList<Integer> pierwszy =new ArrayList<>();
        ArrayList<Integer> drugi =new ArrayList<>();
        pierwszy.add(0,1);
        pierwszy.add(1,4);
        pierwszy.add(2,9);
        pierwszy.add(3,16);
        drugi.add(0,9);
        drugi.add(1,7);
        drugi.add(2,4);
        drugi.add(3,9);
        drugi.add(4,11);
        System.out.println(reversed(pierwszy));
    }
}
