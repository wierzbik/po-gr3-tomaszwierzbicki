package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;

public class Adres {
    private String ulica;
    private int nr_domu;
    private int nr_mieszkania;
    private String miasto;
    private String kod_pocztowy;
    public Adres(String ul,int nr_d,String m,String kod_pocz){
        ulica = ul;
        nr_domu = nr_d;
        miasto = m;
        kod_pocztowy = kod_pocz;
    }
    public Adres(String ul,int nr_d,int nr_m,String m,String kod_pocz) {
        ulica = ul;
        nr_domu = nr_d;
        nr_mieszkania = nr_m;
        miasto = m;
        kod_pocztowy = kod_pocz;
    }
    public void pokaz(Adres dane){
        ulica = dane.ulica;
        nr_domu = dane.nr_domu;
        nr_mieszkania = dane.nr_mieszkania;
        miasto = dane.miasto;
        kod_pocztowy = dane.kod_pocztowy;
        System.out.println("Kod pocztowy: "+kod_pocztowy +" miasto "+miasto);
        System.out.println("Ulica: "+ulica+" Numer domu: "+nr_domu + " numer mieszkania "+nr_mieszkania);
    }

    public static void main(String[] args) {
        Adres adres1 = new Adres("Wojska Polskiego",54,2,"Ełk","19-300");
        Adres adres2 = new Adres("Elcka",33,"Grajewo","19-200");
        adres1.pokaz(adres1);
        adres2.pokaz(adres2);
    }
}