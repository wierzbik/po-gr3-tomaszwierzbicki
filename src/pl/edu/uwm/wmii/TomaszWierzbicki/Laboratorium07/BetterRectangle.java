package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;

public class BetterRectangle extends java.awt.Rectangle {

    public BetterRectangle(int widht, int height) {
        super(widht, height);
    }
    public int getPerimeter() {
        return 2 * width + 2 * height;
    }

    public int getArea() {
        return width * height;
    }
}
class Main{
    public static void main(String[] args) {
        BetterRectangle prostokat = new BetterRectangle(10,5);
        System.out.println("Obwod: "+prostokat.getPerimeter());
        System.out.println("Pole: "+prostokat.getArea());
    }
}