package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;

public class Osoba {
    protected String nazwisko;
    protected int rokUrodzenia;
}

class Student extends Osoba {
    private String kierunek;

    public Student(String nazw, int rokUrodz, String kier) {
        nazwisko = nazw;
        rokUrodzenia = rokUrodz;
        kierunek = kier;
    }

    public String toString() {
        return "Nazwisko: " + nazwisko + " Rok Urodzenia: " + rokUrodzenia + " Kierunek: " + kierunek;
    }

    String getKierunek() {
        return kierunek;
    }

    String getnazwisko() {
        return nazwisko;
    }

    int getrokUrodzenia() {
        return rokUrodzenia;
    }
}

class Nauczyciel extends Osoba {
    private int Pensja;

    public Nauczyciel(String nazw, int rokUrodz, int kasa) {
        nazwisko = nazw;
        rokUrodzenia = rokUrodz;
        Pensja = kasa;
    }

    public String toString() {
        return "Nazwisko: " + nazwisko + " Rok Urodzenia: " + rokUrodzenia + " Pensja: " + Pensja;
    }

    int getPensja() {
        return Pensja;
    }

    String getnazwisko() {
        return nazwisko;
    }

    int getrokUrodzenia() {
        return rokUrodzenia;
    }
}

class main {
    public static void main(String[] args) {
        Student student = new Student("Piotrowski", 1996, "informatyka");
        System.out.println(student.toString());
        Nauczyciel nauczyciel = new Nauczyciel("Bogus", 1989, 3200);
        System.out.println(nauczyciel.toString());
        System.out.println(student.getKierunek());
    }
}