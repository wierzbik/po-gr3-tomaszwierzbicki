package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium00;

public class Zadanie4 {
    public static void main(String[] args) {
        System.out.println("Saldo konta na początku - 1000zl");
        double a = (0.06)*1000;
        double b = a+1000;
        System.out.println("Saldo konta po 1 roku wynosi: " + b );
        double c = (0.06)*b;
        double d = c+b;
        System.out.println("Saldo konta po 2 roku wynosi: " + d);
        double e= (0.06)*d;
        double f = d+e;
        System.out.println("Saldo konta po 3 roku wynosi: " +f);
    }

}
