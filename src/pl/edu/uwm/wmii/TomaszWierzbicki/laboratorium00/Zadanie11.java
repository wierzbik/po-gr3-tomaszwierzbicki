package com.company;

public class Zadanie11 {
    public static void main(String[] args) {
        System.out.println("Nagrobek\n" +
                "\n" +
                "Tu leży staroświecka jak przecinek\n" +
                "autorka paru wierszy. Weczny odpoczynek\n" +
                "raczyła dać jej ziemia, pomimo że trup\n" +
                "nie należał do żadnej z literackich grup.\n" +
                "Ale tez nic lepszego nie ma na mogile\n" +
                "oprócz tej rymowanki, łopianu i sowy.\n" +
                "Przechodniu, wyjmij z teczki mózg elektronowy\n" +
                "i nad losem Szymborskiej podumaj przez chwilę.");
    }
}



