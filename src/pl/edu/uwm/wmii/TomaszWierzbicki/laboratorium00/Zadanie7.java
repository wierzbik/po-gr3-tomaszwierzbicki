package com.company;

public class Zadanie7 {
    public static void Zadanie7(String[] args) {
        System.out.println("***********\n" +
                "     *\n" +
                "     *\n" +
                "     *\n" +
                "     *\n" +
                "     *\n" +
                "     *\n" +
                "     *\n");
        System.out.println(
                "   * *  *  " +
                        "          *\n" +
                        " *         *\n" +
                        "*          *\n" +
                        " *        *\n" +
                        "  *      *\n" +
                        "   *" +
                        "   *");
        System.out.println(
                "*        *\n" +
                        "* *     * *\n" +
                        "*  *   *  *\n" +
                        "*   * *   *\n" +
                        "*    *    *");
        System.out.println(
                "******\n" +
                        "*\n" +
                        "******\n" +
                        "*\n" +
                        "*\n" +
                        "******\n"
        );
        System.out.println(
                "*   *\n" +
                        "* *\n" +
                        "*\n" +
                        "* *\n" +
                        "*   *\n" +
                        "*    *"


        );
    }
}