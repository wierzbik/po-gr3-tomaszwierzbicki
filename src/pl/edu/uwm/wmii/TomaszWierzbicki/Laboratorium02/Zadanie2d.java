package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Random;
import java.util.Scanner;

public class Zadanie2d {
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc) {
        Random rand = new Random();
        if (1 <= n && n <= 100) {
            if (minWartosc >= -999 && maxWartosc <= 999) {
                for (int i = 0; i < n; i++) {

                    int losowe = rand.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
                    tab[i] = losowe;
                    System.out.println(tab[i]);
                }
            }
        }
    }

    public static int sumaDodatnich(int tab[]){
        int suma = 0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]>=0){
                suma = tab[i]+suma;
            }
        }
        return suma;
    }
    public static int sumaUjemnych(int tab[]){
        int suma =0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]<0){
                suma = tab[i]+suma;
            }
        }
        return suma;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int tab[] =  new int[n];
        int minWartosc = scan.nextInt();
        int maxWartosc = scan.nextInt();
        generuj(tab,n,minWartosc,maxWartosc);
        int wynik = sumaDodatnich(tab);
        System.out.println("Suma liczb dodatnich z tablicy wynosi: "+wynik);
        int wynik1 = sumaUjemnych(tab);
        System.out.println("Suma liczb ujemnych z tablicy wynosi: "+wynik1);
    }
}
