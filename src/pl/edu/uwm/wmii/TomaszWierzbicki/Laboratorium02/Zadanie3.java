package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadanie3 {
    public static void Zadanie(int m,int n,int k){
        Random rand = new Random();
        int [][]macierz1= new int[m][n];
        int [][]macierz2=new int[n][k];
        int [][]macierz3=new int[m][k];
        if((m>=1&&m<=10)&&(n>=1&&n<=10)&&(k>=1&&k<=10)) {
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    int losowe = rand.nextInt(10) + 1;
                    macierz1[i][j] = losowe;
                }
            }
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < k; j++) {
                    int losowe = rand.nextInt(10) + 1;
                    macierz2[i][j] = losowe;
                }
            }
            for(int i=0;i<n;i++){
                for(int j=0;j<n;j++){
                    for(int z=0;z<n;z++){
                        macierz3[i][j] = macierz3[i][j] + macierz1[i][z]*macierz2[z][j];
                    }
                }
            }
        }
        System.out.println(Arrays.deepToString(macierz1));
        System.out.println(Arrays.deepToString(macierz2));
        System.out.println(Arrays.deepToString(macierz3));
    }

    public static void main(String[] args) {
        Zadanie3 z = new Zadanie3();
        Scanner scan = new Scanner(System.in);
        int m = scan.nextInt();
        int n = scan.nextInt();
        int k = scan.nextInt();
        z.Zadanie(m,n,k);
    }
}
