package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;
import java.util.Random;


public class Zadanie1f {
    public void zad1f(int liczba)
    {
        int tab[] = new int[liczba];
        Random rand = new Random();
        System.out.println("Wylosowane liczby: ");
        if (1<+liczba && liczba <=100)
        {
            for (int i=0;i<liczba;i++)
            {
                int llosowe = rand.nextInt(999 + 999 -1) - 999;
                tab[i] = llosowe;
                System.out.println(tab[i]);
            }
        }
        System.out.println("Wartosci zmodyfikowane:");
        for (int i =0;i<liczba;i++)
        {
            if(tab[i] >=0)
            {
                tab[i] = 1;
            }
            if(tab[i]<0)
            {
                tab[i] = -1;
            }
            System.out.println(tab[i]);
        }

    }

    public static void main(String[] args) {
        Zadanie1f x = new Zadanie1f();
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        x.zad1f(n);
    }

}