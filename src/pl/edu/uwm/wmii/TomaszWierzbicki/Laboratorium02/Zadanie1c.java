package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;
import java.util.Random;

public class Zadanie1c {
    public void zad1c(int liczba)
    {
        int powtorzenia = 0;
        int tab[] = new int[liczba];
        Random rand = new Random();
        if (liczba>=1 && liczba<=100)
        {
            for (int i=0;i<liczba;i++)
            {
                int llosowe = rand.nextInt(999 + 999 - 1) - 999;
                tab[i] = llosowe;
                System.out.println(tab[i]);
            }
        }
        int najwiekszy = tab[0];
        for (int i=0;i<liczba;i++)
        {
            if (tab[i] > najwiekszy)
            {
                najwiekszy = tab[i];
            }
        }

        int wynik = 0;
        for (int i=0;i<liczba;i++)
        {
            if (najwiekszy == tab[i])
            {
                wynik++;
            }
        }
        System.out.println("Najwiekszy element tablicy: "+najwiekszy+" wystepuje "+wynik+" razy w tablicy");

    }


    public static void main(String[] args) {
        Zadanie1c z = new Zadanie1c();
        Scanner scan = new Scanner(System.in);
        int liczba = scan.nextInt();
        z.zad1c(liczba);


    }
}