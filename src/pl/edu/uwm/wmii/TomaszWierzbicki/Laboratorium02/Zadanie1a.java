package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;
import java.util.Random;

public class Zadanie1a {
    public void zadanie1(int n){
        int parzyste=0;
        int nieparzyste =0;
        int tab[] = new int[n];
        Random rand = new Random();
        if(n>=1&& n <=100) {
            for (int i = 0; i < n; i++) {
                int losowe = rand.nextInt(999 + 999 - 1) - 999;
                tab[i] = losowe;
                System.out.println(tab[i]);
            }
        }
        for(int i=0;i<n;i++) {
            if (tab[i] % 2 == 0) {
                parzyste++;
            }
        }
        for(int i=0;i<n;i++){
            if(tab[i] % 2 != 0 ){
                nieparzyste++;
            }
        }
        System.out.println("W tablicy sa: "+parzyste +" elementy parzyste oraz "+nieparzyste +" elementy nieparzyste");
    }

    public static void main(String[] args) {
        Zadanie1a x = new Zadanie1a();
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        x.zadanie1(n);
    }

}