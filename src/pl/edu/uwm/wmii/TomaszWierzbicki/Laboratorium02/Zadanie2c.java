package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Random;
import java.util.Scanner;

public class Zadanie2c {
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc) {
        Random rand = new Random();
        if (1 <= n && n <= 100) {
            if (minWartosc >= -999 && maxWartosc <= 999) {
                for (int i = 0; i < n; i++) {

                    int losowe = rand.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
                    tab[i] = losowe;
                    System.out.println(tab[i]);
                }
            }
        }
    }

    public static int ileMaksymalnych(int tab[]){
        int max = tab[0];
        for(int i=0;i<tab.length;i++){

            if(tab[i]>max){
                max = tab[i];
            }
        }
        int ile = 0;
        for(int i=0;i<tab.length;i++){
            if(max==tab[i]){
                ile++;
            }
        }
        return ile;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int tab[] =  new int[n];
        int minWartosc = scan.nextInt();
        int maxWartosc = scan.nextInt();
        generuj(tab,n,minWartosc,maxWartosc);
        int wynik = ileMaksymalnych(tab);
        System.out.println("W tablicy wartosc maksymalna wystepuje: "+wynik+" razy");
    }
}
