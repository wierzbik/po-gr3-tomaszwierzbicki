package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Random;
import java.util.Scanner;

public class Zadanie2e {
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc) {
        Random rand = new Random();
        if (1 <= n && n <= 100) {
            if (minWartosc >= -999 && maxWartosc <= 999) {
                for (int i = 0; i < n; i++) {

                    int losowe = rand.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
                    tab[i] = losowe;
                    System.out.println(tab[i]);
                }
            }
        }
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[]){
        int iloscDodatnich = 0;
        int dlugosc = 0;
        int temp[]=new int[tab.length];
        for(int i=0;i<tab.length;i++){
            temp[i] =0;
        }
        for(int i=0;i<tab.length;i++){
            if(tab[i]>=0){
                iloscDodatnich++;
            }
            else{
                temp[i]=iloscDodatnich;
                iloscDodatnich = 0;
            }
        }
        for(int i=0;i<temp.length;i++){
            if(temp[i]>dlugosc){
                dlugosc = temp[i];
            }
        }
        return dlugosc;
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int tab[] =  new int[n];
        int minWartosc = scan.nextInt();
        int maxWartosc = scan.nextInt();
        generuj(tab,n,minWartosc,maxWartosc);
        int wynik = dlugoscMaksymalnegoCiaguDodatnich(tab);
        System.out.println("Dlugosc maksymalnego ciagu liczb dodatnich w tablicy: "+wynik);
    }
}
