package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;
import java.util.Random;


public class Zadanie1e {

    public void zad1e(int liczba)
    {
        int tab[] = new int[liczba];
        int najdluzszy[] = new int[liczba];
        Random rand = new Random();
        int wynik = 0;
        int dlugosc = 0;
        if (1<=liczba && liczba<= 100)
        {
            for (int i=0;i< liczba;i++) //tablice wypelniam zerami
            {
                najdluzszy[i] = 0;
            }
            for (int i =0;i<liczba;i++) // tablice wypelniam liczbami losowymi z przedzialu <-999,999>
            {
                int llosowe = rand.nextInt(999 + 999 -1) - 999;
                tab[i] = llosowe;
                System.out.println(tab[i]);
            }
        }
        for (int i=0;i<liczba;i++)  // zliczam liczby dodatnie
        {
            if (tab[i] >= 0) {
                wynik++;
            } else {
                najdluzszy[i] = wynik; // wrzucam wynik do tablicy
                wynik = 0;
            }
        }
        for (int i=0;i<liczba;i++)
        {
            if(najdluzszy[i] > dlugosc)
            {
                dlugosc = najdluzszy[i] ; // szukam najdluzszego elementu tablicy
            }
        }
        System.out.println("Najdluzszy fragment tablicy zlozony z liczb dodatnich  jest dlugosci "+dlugosc+"");



    }

    public static void main(String[] args) {
        Zadanie1e z = new Zadanie1e();
        Scanner scan = new Scanner(System.in);
        int liczba = scan.nextInt();
        z.zad1e(liczba);


    }
}