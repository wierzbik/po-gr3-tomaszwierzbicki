package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Random;
import java.util.Scanner;


public class Zadanie2b {
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc) {
        Random rand = new Random();
        if (1 <= n && n <= 100) {
            if (minWartosc >= -999 && maxWartosc <= 999) {
                for (int i = 0; i < n; i++) {

                    int losowe = rand.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
                    tab[i] = losowe;
                    System.out.println(tab[i]);
                }
            }
        }
    }
    public static int ileDodatnich(int tab[]){
        int dodatnie=0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]>0){
                dodatnie++;
            }
        }
        return dodatnie;
    }
    public static int ileUjemnych(int tab[]){
        int ujemne=0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]<0){
                ujemne++;
            }
        }
        return ujemne;
    }
    public static int ileZer(int tab[]){
        int zera=0;
        for(int i=0;i<tab.length;i++){
            if(tab[i]==0) {
                zera++;
            }
        }
        return zera;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int tab[] =  new int[n];
        int minWartosc = scan.nextInt();
        int maxWartosc = scan.nextInt();
        generuj(tab,n,minWartosc,maxWartosc);
        int wynik =ileDodatnich(tab);
        int wynik1 = ileUjemnych(tab);
        int wynik2 = ileZer(tab);
        System.out.println("W tablicy znajduje sie "+wynik+" liczb dodatnich "+wynik1+" liczb ujemnych i "+wynik2+" zer");

    }
}
