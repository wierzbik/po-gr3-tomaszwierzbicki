package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Random;
import java.util.Scanner;

public class Zadanie2a {
    public static void generuj(int tab[], int n, int min, int max) {
        Random rand = new Random();
        if (1 <= n && n <= 100) {
            if (min >= -999 && max <= 999) {
                for (int i = 0; i < n; i++) {

                    int losowe = rand.nextInt(max - min + 1) + min;
                    tab[i] = losowe;
                    System.out.println(tab[i]);
                }
            }
        }
    }

    public static int ileNieparzystych(int tab[]){
        int nieparzyste=0;
        for(int i=0;i<tab.length;i++){
            if(tab[i] % 2 != 0){
                nieparzyste++;
            }
        }
        return nieparzyste;
    }
    public static int ileParzystych(int tab[]){
        int Parzyste=0;
        for(int i=0;i<tab.length;i++){
            if(tab[i] % 2==0){
                Parzyste++;
            }
        }
        return Parzyste;
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int tab[] =  new int[n];
        int min = scan.nextInt();
        int max = scan.nextInt();
        generuj(tab,n,min,max);
        int wynik = ileNieparzystych(tab);
        System.out.println("Liczb nieparzystych w tablicy : "+wynik);
        int wynik1 = ileParzystych(tab);
        System.out.println("Liczb parzystych w tablicy : "+wynik1);

    }
}
