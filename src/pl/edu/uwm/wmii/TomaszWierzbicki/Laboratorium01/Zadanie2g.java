package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.lang.*;
import java.util.Scanner;

public class Zadanie2g {
    public void Zad2g(int liczba){
        int wynik =0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int wartosc = x.nextInt();
            if((wartosc % 2!=0)&& wartosc >=0){
                wynik++;
            }
        }
        System.out.println("Liczby nieparzyste i nieujemne: "+wynik);
    }

    public static void main(String[] args) {
        Zadanie2g z = new Zadanie2g();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.Zad2g(liczba);

    }
}