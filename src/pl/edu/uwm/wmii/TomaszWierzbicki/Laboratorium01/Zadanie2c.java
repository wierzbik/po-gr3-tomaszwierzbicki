package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.lang.*;
import java.util.Scanner;

public class Zadanie2c {

    public void Zad2c(int liczba){
        int wynik =0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int wartosc = x.nextInt();
            if((Math.sqrt(wartosc)) % 2 == 0){
                wynik++;
            }
        }
        System.out.println("Kwadratow liczb parzystych jest: "+wynik);
    }

    public static void main(String[] args) {
        Zadanie2c z = new Zadanie2c();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.Zad2c(liczba);

    }
}
