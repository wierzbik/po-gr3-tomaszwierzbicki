package com.company;
import java.util.*;
import java.util.Scanner;

public class Samochod {
    String marka;
    int rocznik;
    float pojSilnika;
    public void Ograniczenia(){
        if(rocznik <=1900 || rocznik >=2018){
            System.out.println("Bledny rocznik");
        }
    }
    public void SpalanieNaTrasie(int trasa){
        float wynik = trasa*5*pojSilnika;
        System.out.println("Spalanie na trasie wynosi: "+wynik);
    }

    public static void main(String[] args) {
        Samochod pierwszy = new Samochod();
        pierwszy.marka= "Toyota";
        pierwszy.rocznik=2008;
        pierwszy.pojSilnika=2;
        Scanner scan = new Scanner(System.in);
        int trasa = scan.nextInt();
        pierwszy.SpalanieNaTrasie(trasa);

        Samochod drugi = new Samochod();
        drugi.marka = "Jaguar";
        drugi.rocznik = 1998;
        drugi.pojSilnika = 3.2F;

        Samochod trzeci = new Samochod();
        trzeci.marka = "Audi";
        trzeci.rocznik = 2000;
        trzeci.pojSilnika = 1.6F;

        String tab[] = new String[3];
        tab[0]=pierwszy.marka;
        tab[1]=drugi.marka;
        tab[2]=trzeci.marka;
    }
}