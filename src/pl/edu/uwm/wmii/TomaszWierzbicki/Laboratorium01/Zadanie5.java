package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;

public class Zadanie5 {
    public void zad5(int liczba){
        int tab[]= new int[liczba];
        int wynik=0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int wartosc = x.nextInt();
            tab[i]=wartosc;
        }
        for(int i=0;i<liczba;i++){
            if(i<liczba-1&&(tab[i]>0 && tab[i+1]>0)){
                wynik++;
            }
        }
        System.out.println(wynik);
    }

    public static void main(String[] args) {
        Zadanie5 z = new Zadanie5();
        Scanner scan = new Scanner(System.in);
        int liczba = scan.nextInt();
        z.zad5(liczba);
    }
}