package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.lang.*;
import java.util.Scanner;
public class Zadanie2e {

    public long silnia(int j){
        if(1>j){
            return 1;
        }
        else{
            return j*silnia(j-1);
        }
    }
    public void Zad2e(int liczba){
        int wynik=0;
        for(int i=1;i<=liczba;i++){
            Scanner x = new Scanner(System.in);
            int wartosc = x.nextInt();
            if(((Math.pow(2,i))<wartosc)&&(wartosc < silnia(i))){
                wynik++;
            }
        }
        System.out.println("Liczby ktore spelniaja warunek:  "+wynik);
    }


    public static void main(String[] args) {
        Zadanie2e z = new Zadanie2e();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.Zad2e(liczba);

    }
}