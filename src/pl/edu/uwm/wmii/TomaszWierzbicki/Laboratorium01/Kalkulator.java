package com.company;
import java.util.*;

public class Kalkulator {
    public void dodaj(int a, int b) {
        int wynik = 0;
        wynik = a + b;
        System.out.println(wynik);
    }

    public void odejmij(int a, int b) {
        int wynik = 0;
        wynik = a - b;
        System.out.println(wynik);
    }

    public void pomnoz(int a, int b) {
        int wynik = 0;
        wynik = a * b;
        System.out.println(wynik);
    }

    public void podziel(float a, float b) {
        float wynik = 0;
        if (b != 0) {
            wynik = a / b;
            System.out.println(wynik);
        } else
            System.out.println("Nie dzielimy przez 0");
    }

    public void podzielZreszta(int a, int b) {
        int wynik = 0;
        wynik = a % b;
        System.out.println(wynik);
    }

    public void spierwiastkuj(int a) {
        double wynik = Math.sqrt(a);
        System.out.println(wynik);
    }


    public static void main(String[] args) {
        Kalkulator k = new Kalkulator();
        k.podzielZreszta(25, 20);
        k.spierwiastkuj(16);
    }
}