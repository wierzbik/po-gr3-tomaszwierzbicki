package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;
import java.lang.*;

public class Zadanie1G {
    public void zad1g(int liczba) {
        int wynikSumy = 0;
        int wynikIloczynu =0;
        for (int i = 0; i < liczba; i++) {
            Scanner x = new Scanner(System.in);
            int suma = x.nextInt();
            int iloczyn = suma;
            wynikSumy += suma;
            if (i == 0) {
                wynikIloczynu += iloczyn;
            }
            else{
                wynikIloczynu *= iloczyn;
            }
        }
        System.out.println("Suma: "+ wynikSumy);
        System.out.println("Iloczyn: "+ wynikIloczynu);
    }

    public static void main(String[] args) {
        Zadanie1G z = new Zadanie1G();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.zad1g(liczba);
    }
}
