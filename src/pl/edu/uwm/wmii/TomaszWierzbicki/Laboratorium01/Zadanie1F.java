package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;
import java.lang.*;

public class Zadanie1F {
    public void zad1f(int liczba){
        int wynik=0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int suma = x.nextInt();
            wynik += Math.pow(suma,2);
        }
        System.out.println(wynik);
    }

    public static void main(String[] args) {
        Zadanie1F z = new Zadanie1F();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.zad1f(liczba);

    }
}
