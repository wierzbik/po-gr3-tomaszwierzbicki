package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.lang.*;
import java.util.Scanner;


public class Zadanie2h {

    public void Zad2h(int liczba){
        int wynik=0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int wartosc = x.nextInt();
            if(Math.abs(wartosc)<Math.pow(i,2)) {
                wynik++;
            }
        }
        System.out.println("Liczby ktore spelniaja warunek: "+wynik);
    }

    public static void main(String[] args) {
        Zadanie2h z = new Zadanie2h();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.Zad2h(liczba);
    }
}