package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;
import java.lang.*;

public class Zadanie1D {

    public void zad4d(int liczba){
        float wynik=0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int suma = x.nextInt();
            wynik += Math.sqrt(Math.abs(suma));
        }
        System.out.println(wynik);
    }

    public static void main(String[] args) {
        Zadanie1D z = new Zadanie1D();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.zad4d(liczba);
    }
}
