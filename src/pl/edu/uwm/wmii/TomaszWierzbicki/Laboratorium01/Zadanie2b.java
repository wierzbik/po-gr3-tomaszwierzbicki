package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.lang.*;
import java.util.Scanner;

public class Zadanie2b {

    public void Zad2b(int liczba){
        int wynik =0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int wartosc = x.nextInt();
            if((wartosc % 3 ==0)&&(wartosc % 5 !=0)){
                wynik++;
            }
        }
        System.out.println("Liczb ktore dziela sie przez 3 i nie dziela przez 5 jest:: "+wynik);
    }

    public static void main(String[] args) {
        Zadanie2b z = new Zadanie2b();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.Zad2b(liczba);

    }
}
