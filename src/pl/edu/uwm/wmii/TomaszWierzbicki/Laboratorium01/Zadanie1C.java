package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;
import java.lang.*;

public class Zadanie1C {
    public void zad3c(int liczba)
    {
        int wynik = 0;
        for (int i=0;i<liczba;i++)
        {
            Scanner x = new Scanner(System.in);
            int suma = x.nextInt();
            wynik += Math.abs(suma);
        }
        System.out.println(wynik);
    }

    public static void main(String[] args) {
        Zadanie1C z = new Zadanie1C();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.zad3c(liczba);
    }
}
