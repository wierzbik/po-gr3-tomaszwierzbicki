package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;
import java.lang.*;

public class Zadanie1E {
    public void zad1e(int liczba){
        int wynik=0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int iloczyn = x.nextInt();
            if(i==0){
                wynik += Math.abs(iloczyn);
            }
            else {
                wynik *= Math.abs(iloczyn);
            }
        }
        System.out.println(wynik);
    }

    public static void main(String[] args) {
        Zadanie1E z = new Zadanie1E();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.zad1e(liczba);
    }
}
