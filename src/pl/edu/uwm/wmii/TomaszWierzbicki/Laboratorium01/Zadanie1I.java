package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;
import java.lang.*;
public class Zadanie1I {
    public void zad1i(int liczba){
        float wynik=0;
        for(int i=1;i<=liczba;i++){
            Scanner x = new Scanner(System.in);
            int suma = x.nextInt();
            wynik += (suma/silnia(i))*Math.pow(-1,i);
        }
        System.out.println(wynik);
    }
    public long silnia(int j){
        if(1>j){
            return 1;
        }
        else{
            return j*silnia(j-1);
        }
    }
    public static void main(String[] args) {
        Zadanie1I z = new Zadanie1I();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.zad1i(liczba);

    }
}
