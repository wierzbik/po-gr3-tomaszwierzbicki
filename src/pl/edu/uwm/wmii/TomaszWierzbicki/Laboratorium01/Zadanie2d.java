package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.lang.*;
import java.util.Scanner;


public class Zadanie2d {

    public void Zad2d(int liczba){
        int tab[] = new int[liczba];
        int wynik =0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int wartosc = x.nextInt();
            tab[i]=wartosc;
        }
        for(int i=0;i<liczba;i++){
            if((i>0)&&(i<liczba-1)&&(tab[i]<(tab[i-1]+tab[i+1])/2))
            {
                wynik++;
            }
        }
        System.out.println("Liczb spelniajacych warunek jest: "+ wynik);
    }


    public static void main(String[] args) {
        Zadanie2d z = new Zadanie2d();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.Zad2d(liczba);

    }
}
