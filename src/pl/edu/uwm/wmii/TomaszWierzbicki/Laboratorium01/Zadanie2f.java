package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.lang.*;
import java.util.Scanner;
public class Zadanie2f {

    public void Zad2f(int liczba){
        int wynik =0;
        for(int i=1;i<=liczba;i++){ // i=1 numerujemy od 1 do n
            Scanner x =new Scanner(System.in);
            int wartosc = x.nextInt();
            if(i%2 !=0 && wartosc % 2 ==0){
                wynik++;
            }
        }
        System.out.println("Liczby parzyste majace nieparzysty numer: "+wynik);
    }

    public static void main(String[] args) {
        Zadanie2f z = new Zadanie2f();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.Zad2f(liczba);

    }
}