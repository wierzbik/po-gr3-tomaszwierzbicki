package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;

public class Zadanie3 {
    public void Zad3(int liczba){
        int dodatnie=0;
        int ujemne=0;
        int zer=0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int wartosc = x.nextInt();
            if(wartosc>0){
                dodatnie++;
            }
            if(wartosc<0){
                ujemne++;
            }
            if(wartosc==0){
                zer++;
            }
        }
        System.out.println("Liczby dodatnie: "+dodatnie+",ujemne: "+ujemne+",zera: "+zer);
    }

    public static void main(String[] args) {
        Zadanie3 z = new Zadanie3();
        Scanner scan = new Scanner(System.in);
        int liczba =scan.nextInt();
        z.Zad3(liczba);
    }
}