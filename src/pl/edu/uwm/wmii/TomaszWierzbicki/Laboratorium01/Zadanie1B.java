package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;
import java.lang.*;

public class Zadanie1B{
    public void zadanie1b(int liczba){
        int wynik=0;
        for(int i=0;i<liczba;i++){
            Scanner x = new Scanner(System.in);
            int iloczyn = x.nextInt();
            if(i==0){
                wynik += iloczyn;
            }
            else {
                wynik *= iloczyn;
            }
        }
        System.out.println(wynik);
    }

    public static void main(String[] args) {
        Zadanie1B z = new Zadanie1B();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.zadanie1b(liczba);
    }
}
