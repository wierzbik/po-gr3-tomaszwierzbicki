package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.util.Scanner;
import java.lang.*;
public class Zadanie1A {
    public void zadanie1a(int n)
    {
        int wynik = 0;
        for (int i=0;i<n;i++)
        {
            Scanner skan = new Scanner(System.in);
            int suma = skan.nextInt();
            wynik += suma;

        }
        System.out.println(wynik);
    }

    public static void main(String[] args) {

        Zadanie1A x = new Zadanie1A();
        Scanner skan = new Scanner(System.in);
        int n = skan.nextInt();
        x.zadanie1a(n);
    }
}
