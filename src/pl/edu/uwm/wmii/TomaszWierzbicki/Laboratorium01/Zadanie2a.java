package pl.edu.uwm.wmii.TomaszWierzbicki.laboratorium01;
import java.lang.*;
import java.util.Scanner;

public class Zadanie2a {
    public void Zad2a (int liczba) {
        int wynik = 0;
        for (int i = 0; i < liczba; i++) {
            Scanner x = new Scanner(System.in);
            int wartosc = x.nextInt();
            if (wartosc % 2 != 0) {
                wynik++;
            }
        }
        System.out.println("liczb nieparzystych jest: " + wynik);
    }

    public static void main(String[] args) {
        Zadanie2a z = new Zadanie2a();
        Scanner scaner = new Scanner(System.in);
        int liczba = scaner.nextInt();
        z.Zad2a(liczba);
    }
}

